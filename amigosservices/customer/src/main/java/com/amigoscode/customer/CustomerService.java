package com.amigoscode.customer;

import org.springframework.stereotype.Service;

@Service
public record CustomerService(CustomerRepository customerRepository) {

	public Customer registerCustomer(CustomerRegistrationRequest customerRegistrationRequest) {
		
	Customer customer = Customer.builder()
		.fristName(customerRegistrationRequest.fristName())
		.lastName(customerRegistrationRequest.lastName())
		.email(customerRegistrationRequest.email())
		.build();
		
		return customerRepository.save(customer);
		//TODO: validar campos
	}

}
