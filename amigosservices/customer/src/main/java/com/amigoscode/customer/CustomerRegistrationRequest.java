package com.amigoscode.customer;

public record CustomerRegistrationRequest(
		String fristName, 
		String lastName, 
		String email) {

}
